package norris.cory.networks.a3;
public class Parse {
	public static Exception InvalidIp = new Exception("Exception: Invalid Host Ip");
	public static Exception InvalidPort = new Exception("Exception: Invalid Port");
	public static Exception InvalidRNumber = new Exception(
			"Exception: Invalid Reliability Number");
	public static Exception InvalidWinSize = new Exception(
			"Exception: Invalid Window Size");
	

	public Parse() throws Exception {
	}

	//Check if an IP is properly formatted and in range
	public static String parseIp(String sIp) throws Exception {
		try {
			String[] parts = sIp.split("\\.");

			for (String s : parts) {
				int i = Integer.parseInt(s);
				if (i < 0 || i > 255)
					throw InvalidIp;

			}
		} catch (Exception e) {
			throw InvalidIp;
		}
		return sIp;
	}

	//Check if a port is an integer and within range
	public static Integer parsePort(String sPort) throws Exception {
		int port;
		try {
			port = Integer.parseInt(sPort);
			if (port < 0 || port > 65535)
				throw InvalidPort;
		} catch (Exception e) {
			throw InvalidPort;
		}
		return port;
	}
	
	//check if the ReceiverNumber is valid
	public static Integer parseR(String sR) throws Exception
	{
		int rNumber;
		try {
			rNumber = Integer.parseInt(sR);
			if (rNumber < 0 )
				throw InvalidRNumber;
		} catch (Exception e) {
			throw InvalidRNumber;
		}
		return rNumber;
	}
	
	//check if the WindowSize is valid
	public static Integer parseWinSize(String sWinSize) throws Exception
	{
		int winSize;
		try {
			winSize = Integer.parseInt(sWinSize);
			if (winSize < 0 || winSize > 128) 
				throw InvalidWinSize;
		} catch (Exception e) {
			throw InvalidWinSize;
		}
		return winSize;
	}

}

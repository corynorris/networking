package norris.cory.networks.a3;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.*;


public class Receiver {
	private InetAddress address;
	private DatagramSocket socket = null;
	private DatagramPacket packet;
	private FileOutputStream fos;
	private int recPort;
	private int sendPort;
	private final Integer MAXSIZE = 124;
	private final Integer HEADERSIZE = 4;

	public static void main(String[] args) throws Exception {

		if (args.length != 4)
			throw new Exception("All 4 arguments are mandatory");

		// get a datagram socket
		InetAddress recIp = InetAddress.getByName(args[0]);
		Integer sendPort = Parse.parsePort(args[1]); // <UDP port number used by the sender to receive ACKs from the receiver
		Integer recPort = Parse.parsePort(args[2]);	 // <UDP port number used by the receiver to receive data from the sender	
		File toSend = new File(args[3]);

		
		@SuppressWarnings("unused")
		Receiver r = new Receiver(recIp, sendPort, recPort, toSend);

	}

	/**
	 * Create the application.
	 * 
	 * @throws IOException
	 */
	public Receiver(InetAddress recIp, Integer sendPort, Integer recPort, File toReceive) throws IOException {
		this.recPort = recPort;
		this.address = recIp;
		// setup our datagram socket
		System.out.println("Receiving on: " + this.address + ":" + this.recPort);
		this.socket = new DatagramSocket(this.recPort, this.address);		
		this.fos = new FileOutputStream(toReceive);
		this.sendPort = sendPort;
		

		// create our buffer
		byte[] buf = new byte[MAXSIZE];
			
		int rN = 0; //receipt number for error checking.
		int header = 0;
		boolean run = true;
		
		
		//until we receive a EOF signal, receive data.  
		do {
			
			// get response
			packet = new DatagramPacket(buf, buf.length);
			socket.receive(packet);	
			header = byteArrayToInt(packet.getData());
			
			//check if we received an EOF message
			if  ( header == -1){ 
				run = false;
			} else if (header  == rN) {//Accept and write the packet #ALSO USE CHECKSUM OF SOME SORT			

				fos.write(packet.getData(),HEADERSIZE,packet.getLength()-HEADERSIZE);
				sendAck(rN);
				rN++;
			}
			
	
			
		}while (run);	
		
		System.out.println("File Transfer Complete");
		socket.close();
		fos.close();
	}
	private void sendAck(int rN) throws IOException {
		byte[] ack = new byte[HEADERSIZE];
		//convert integer to byte array
		ack[0] = (byte) (rN >>> 24);
		ack[1] = (byte) (rN >>> 16);
		ack[2] = (byte) (rN >>> 8);	
		ack[3] = (byte) (rN);
		DatagramPacket packet = new DatagramPacket(ack, HEADERSIZE, this.address, this.sendPort);
		socket.send(packet);

	}
	
	public static final int byteArrayToInt(byte[] b) {
		return (b[0] << 24) + ((b[1] & 0xFF) << 16) + ((b[2] & 0xFF) << 8)
				+ (b[3] & 0xFF);
	}

}

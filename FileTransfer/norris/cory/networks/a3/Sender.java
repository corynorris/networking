package norris.cory.networks.a3;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

public class Sender {
	private InetAddress address;
	private DatagramSocket socket = null;
	private DatagramPacket packet;
	private FileInputStream fis;
	private int rNum;
	private int winSize;
	private int recPort;
	private final Integer MAXSIZE = 124;
	private final Integer HEADERSIZE = 4;
	private final Integer EOF = -1;

	/**
	 * Launch the application.
	 * 
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		// Verify all input
		if (args.length != 6)
			throw new Exception("All 6 arguments are mandatory");
		InetAddress recIp = InetAddress.getByName(args[0]);
		Integer recPort = Parse.parsePort(args[1]); // <UDP port number used by
													// the receiver to receive
													// data from the sender>
		Integer sendPort = Parse.parsePort(args[2]); // <UDP port number used by
														// the sender to receive
														// ACKs from the
														// receiver>
		File toSend = new File(args[3]);
		Integer rNum = Parse.parseR(args[4]);
		Integer winSize = Parse.parseWinSize(args[5]);

		long start = System.nanoTime();

		Sender fileTransfer = new Sender(recIp, recPort, sendPort, toSend,
				rNum, winSize);
		fileTransfer.sendFile();
		long end = System.nanoTime();
		long ttt = end - start;
		System.out.format("Total Transfer Time: %d milliseconds", ttt);
	}

	/**
	 * Create the application.
	 * 
	 * @throws IOException
	 */
	public Sender(InetAddress recIp, Integer recPort, Integer sendPort,
			File toSend, Integer rNum, Integer winSize) throws IOException {

		// setup our datagram socket
		socket = new DatagramSocket(sendPort);
		address = recIp;
		fis = new FileInputStream(toSend);
		this.rNum = rNum;
		this.winSize = winSize;
		this.recPort = recPort;

	}

	public void sendFile() throws IOException {

		ArrayList<DatagramPacket> list = new ArrayList<DatagramPacket>();
		byte[] buf = new byte[MAXSIZE]; // we need 1 byte to
		int sN = 0; // Sequence Number
		int sB = 0; // Sequence Base

		// Read the file into a ArrayList of bytes
		for (int bytesRead; (bytesRead = fis.read(buf, HEADERSIZE, MAXSIZE - HEADERSIZE)) != -1;) {
			list.add(createPacket(buf, sN, bytesRead));
			buf = new byte[MAXSIZE];
			sN++;
		}
		System.out.println("Total Packets to Send: " + list.size());
		sN = 0;

		// until we receive the last ack
		while (sB < list.size()) {

			// if there is still room in our window, send a packet
			if ((sN < sB + winSize) && (sN < list.size())) {

	
				byte[] temp = new byte[MAXSIZE];
				temp = list.get(sN).getData();
				int header = byteArrayToInt(temp); //gets the first integer	
				send(list.get(sN));
				sN++;
			} else {// increase base (sB) when we receive a confirmation


				packet = new DatagramPacket(buf, buf.length);

				// start a timeout
				socket.setSoTimeout(100);
				int ack = -1;
				try {
					socket.receive(packet);
					ack = byteArrayToInt(packet.getData());

					// confirm the ack, increase the base if confirmed
					if (ack == sB)
						sB++;
				} catch (SocketTimeoutException ex) {

					sN = sB;
				}

			}

		}

		// Indicate that we've hit the end of the file.		
		packet = createPacket(buf, EOF, 0);
		socket.send(packet);

		System.out.println("File Transfer Complete");
		socket.close();
		fis.close();
	}

	private DatagramPacket createPacket(byte[] buf, int SequenceNumber,
			int bytesRead) {
		
		//convert integer to byte array
		buf[0] = (byte) (SequenceNumber >>> 24);
		buf[1] = (byte) (SequenceNumber >>> 16);
		buf[2] = (byte) (SequenceNumber >>> 8);	
		buf[3] = (byte) (SequenceNumber);
		
		return new DatagramPacket(buf, MAXSIZE, this.address, this.recPort);

	}

	static int i = 1;

	private void send(DatagramPacket packet) throws IOException {
		// only send packets that are not modulus rNum
		if (rNum == 0)
			socket.send(packet);
		else if ((i % this.rNum) != 0)
			socket.send(packet);
		else
			System.out.println("Dropped packet");
		i++;
	}


	public static final int byteArrayToInt(byte[] b) {
		return (b[0] << 24) + ((b[1] & 0xFF) << 16) + ((b[2] & 0xFF) << 8)
				+ (b[3] & 0xFF);
	}
}

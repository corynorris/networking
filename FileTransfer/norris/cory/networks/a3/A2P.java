package norris.cory.networks.a3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.SortedMap;

public class A2P extends Thread{

	private Socket socket = null;
	private SortedMap<String, Integer> data;

	/*
	 * ErrorCode : Message | Cause
		------------------------------
		  : <Query Result> | A query was successful
		Assignment 1 Protocol (A1P) 1/30/2012 8:11 PM
		1 : No input | A Post or a get command was sent on its own
		2 : No Data | A Get command has no data to return
		3 : Incorrect Command | Something other than a Post or Get command was
	 */
	public static String SUCCESS = " ";
	public static String NO_INPUT = "1No Input"; 
	public static String NO_DATA = "2No Data";
	public static String INCORRECT_COMMAND = "3Incorrect Command";
	
	public A2P(Socket clientSocket, SortedMap<String, Integer> data) {
		super("A1P");
		this.socket = clientSocket;
		this.data = data;
	
	}

	
	
	@Override
	public void run() {
		try {

			// Initialize in/out streams
			PrintWriter out = new PrintWriter(socket.getOutputStream(),
					true);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));

			//Let the client know a connection was established
			out.println("Connection Established");
			
			
			String inputLine, outputLine;

			while ((inputLine = in.readLine()) != null) {
				if (inputLine.equals("exit"))
					break;
				// interpret the input line(s) and prepare an output
				outputLine = interpretInput(inputLine);
				out.println(outputLine);
				
			}
			out.close();
			in.close();
			socket.close();
		} catch (IOException e) {
					
		}

	}

	/** interpretInput
	 * Splits the string given by the client and interprets the given command
	 * returning the appropriate error code or output
	 * @param input The command from the Client
	 * @return The error code or output
	 */
	public String interpretInput(String input) {		
			
		String[] parts = input.split(" ", 0);
		String output = "";
		
				
		//Return an error if there is a command and no input
		if (parts.length < 2) { return NO_INPUT; };
				
		
		switch (parts[0].toUpperCase()) {
			//Add the data to the sorted map
			case "POST":
				for (int i = 1; i < parts.length; i++)				
					addToMap(parts[i]);				
				break;
							
				
			//Get data from the SortedMap	
			case "GET": 
				if (isInteger(parts[1])){
					output = getFromMap(Integer.parseInt(parts[1]));
				}else{
					output = getFromMap(parts[1]);
				}
				if (output == "") { return NO_DATA; }
				break;
				
								
			//Handle Generic Errors
			default:
				//Return an error if there is an incorrect command
				output = INCORRECT_COMMAND;
				break;
		}
		
		
		//Return an error if there is no data to return for the requested command
		
				
		return SUCCESS + output;

	}
	
	/** addToMap
	 * adds a key to a map OR increments the value of they key already there by 1
	 * @param key The key to add to the map
	 */
	/**
	 * @param key
	 */
	private synchronized void addToMap(String key)
	{		
		
		data.put(key, (data.get(key)==null ? 1:data.get(key)+1) );
	}
	
	
	/** getFromMap
	 * returns all words that begin with any of the characters in the given string
	 * @param query String of characters
	 * @return
	 */
	private synchronized String getFromMap(String query)
	{
		StringBuilder toReturn = new StringBuilder();
		for (int i = 0; i < query.length(); i++) {
			for (SortedMap.Entry<String,Integer> entry : data.entrySet())	{
				if (entry.getKey().charAt(0) == query.charAt(i) )
				{
					toReturn.append(entry.getKey());
					toReturn.append(" ");
				}
			}
		}
		return toReturn.toString();
	}	
	/** getFromMap
	 * returns all words that appear more than the specified number
	 * @param query The number
	 * @return
	 */
	private synchronized String getFromMap(Integer query)
	{
		StringBuilder toReturn = new StringBuilder();
		for (SortedMap.Entry<String,Integer> entry : data.entrySet())	{
			if (entry.getValue() >= query)
			{
				toReturn.append(entry.getKey());
				toReturn.append(" ");
			}
		}
		return toReturn.toString();
	}
	
		

    
    
	/** Checks if a string is an integer
	 * @param input
	 * @return Returns true if the string is an integer
	 */
	public boolean isInteger( String input )  
	{  
	   try  
	   {  
	      Integer.parseInt( input );  
	      return true;  
	   }  
	   catch( Exception ex)  
	   {  
	      return false;  
	   }  
	} 
	

}

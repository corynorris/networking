package norris.cory.networks.a1;
import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.net.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Client {

	private JFrame frame;
	private JTextField tbxIp;
	private JTextField tbxPort;
	private JTextField tbxPost;
	private JTextArea textArea;
	private JTextField tbxGet;
	private JButton btnConnect;
	private JButton btnPost;
	private JButton btnGet;
	private boolean connected = false;
	private Socket clientSocket = null;
	PrintWriter out = null;
	BufferedReader in = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Client window = new Client();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Client() {
		initialize();
		registerActions();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();

		frame.setBounds(100, 100, 353, 358);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		// Setup Connection View
		JPanel pnlConnection = new JPanel();
		pnlConnection.setBorder(new EmptyBorder(10, 10, 10, 10));
		frame.getContentPane().add(pnlConnection, BorderLayout.NORTH);
		GridBagLayout gbl_pnlConnection = new GridBagLayout();
		pnlConnection.setLayout(gbl_pnlConnection);

		JLabel lblIp = new JLabel("Ip:");
		GridBagConstraints gbc_lblIp = new GridBagConstraints();
		gbc_lblIp.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblIp.fill = GridBagConstraints.BOTH;
		gbc_lblIp.insets = new Insets(0, 0, 5, 5);
		gbc_lblIp.gridx = 0;
		gbc_lblIp.gridy = 0;
		pnlConnection.add(lblIp, gbc_lblIp);

		tbxIp = new JTextField();
		lblIp.setLabelFor(tbxIp);
		GridBagConstraints gbc_tbxIp = new GridBagConstraints();
		gbc_tbxIp.gridx = 1;
		gbc_tbxIp.gridy = 0;
		gbc_tbxIp.fill = GridBagConstraints.BOTH;
		gbc_tbxIp.anchor = GridBagConstraints.NORTHWEST;
		gbc_tbxIp.weightx = 1.0;
		gbc_tbxIp.gridwidth = GridBagConstraints.REMAINDER;
		gbc_tbxIp.insets = new Insets(1, 1, 5, 1);
		pnlConnection.add(tbxIp, gbc_tbxIp);
		tbxIp.setColumns(10);

		JLabel lblPort = new JLabel("Port:");
		GridBagConstraints gbc_lblPort = new GridBagConstraints();
		gbc_lblPort.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblPort.fill = GridBagConstraints.BOTH;
		gbc_lblPort.insets = new Insets(0, 0, 5, 5);
		gbc_lblPort.gridx = 0;
		gbc_lblPort.gridy = 1;
		pnlConnection.add(lblPort, gbc_lblPort);

		tbxPort = new JTextField();
		lblPort.setLabelFor(tbxPort);
		GridBagConstraints gbc_tbxPort = new GridBagConstraints();
		gbc_tbxPort.gridx = 1;
		gbc_tbxPort.gridy = 1;
		gbc_tbxPort.fill = GridBagConstraints.BOTH;
		gbc_tbxPort.anchor = GridBagConstraints.NORTHWEST;
		gbc_tbxPort.weightx = 1.0;
		gbc_tbxPort.gridwidth = GridBagConstraints.REMAINDER;
		gbc_tbxPort.insets = new Insets(1, 1, 5, 1);
		pnlConnection.add(tbxPort, gbc_tbxPort);
		tbxPort.setColumns(10);

		btnConnect = new JButton((connected)?"Disconnect":"Connect");
		GridBagConstraints gbc_btnConnect = new GridBagConstraints();
		gbc_btnConnect.anchor = GridBagConstraints.EAST;
		gbc_btnConnect.gridx = 1;
		gbc_btnConnect.gridy = 2;
		pnlConnection.add(btnConnect, gbc_btnConnect);

		// Add the post button and text field

		JPanel pnlPost = new JPanel();
		pnlPost.setBorder(new EmptyBorder(10, 10, 10, 10));
		frame.getContentPane().add(pnlPost, BorderLayout.SOUTH);
		GridBagLayout gbl_pnlPost = new GridBagLayout();
		pnlPost.setLayout(gbl_pnlPost);

		tbxPost = new JTextField();
		GridBagConstraints gbc_tbxPost = new GridBagConstraints();
		gbc_tbxPost.gridx = 0;
		gbc_tbxPost.gridy = 0;
		gbc_tbxPost.fill = GridBagConstraints.BOTH;
		gbc_tbxPost.anchor = GridBagConstraints.NORTHWEST;
		gbc_tbxPost.weightx = 1.0;
		pnlPost.add(tbxPost, gbc_tbxPost);
		tbxPost.setColumns(10);

		btnPost = new JButton("Post");
		btnPost.setEnabled(connected);
		GridBagConstraints gbc_btnPost = new GridBagConstraints();
		gbc_btnPost.gridx = 1;
		gbc_btnPost.gridy = 0;
		gbc_btnPost.anchor = GridBagConstraints.NORTHEAST;
		gbc_btnPost.fill = GridBagConstraints.BOTH;
		pnlPost.add(btnPost, gbc_btnPost);

		// Add the display for GET
		JPanel pnlGet = new JPanel();
		pnlGet.setBorder(new EmptyBorder(0, 10, 0, 10));
		frame.getContentPane().add(pnlGet, BorderLayout.CENTER);
		GridBagLayout gbl_pnlGet = new GridBagLayout();
		gbl_pnlGet.rowWeights = new double[] { 1.0, 0.0 };
		gbl_pnlGet.columnWeights = new double[] { 1.0, 0.0 };
		pnlGet.setLayout(gbl_pnlGet);

		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setEditable(false);
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.gridwidth = 2;
		gbc_textArea.insets = new Insets(0, 0, 5, 5);
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridx = 0;
		gbc_textArea.gridy = 0;
		pnlGet.add(textArea, gbc_textArea);

		tbxGet = new JTextField();		
		GridBagConstraints gbc_tbxGet = new GridBagConstraints();
		gbc_tbxGet.insets = new Insets(0, 0, 0, 5);
		gbc_tbxGet.gridx = 0;
		gbc_tbxGet.gridy = 1;
		gbc_tbxGet.fill = GridBagConstraints.BOTH;
		gbc_tbxGet.anchor = GridBagConstraints.NORTHWEST;
		gbc_tbxGet.weightx = 1.0;
		pnlGet.add(tbxGet, gbc_tbxGet);
		tbxGet.setColumns(10);

		btnGet = new JButton("Get");
		btnGet.setEnabled(connected);
		GridBagConstraints gbc_btnGet = new GridBagConstraints();
		gbc_btnGet.gridx = 1;
		gbc_btnGet.gridy = 1;
		gbc_btnGet.anchor = GridBagConstraints.NORTHEAST;
		gbc_btnGet.fill = GridBagConstraints.BOTH;
		pnlGet.add(btnGet, gbc_btnGet);

		
		
	}

	private void registerActions() {

		btnConnect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					textArea.setText("");
					if (connected) {
						out.println("exit");
						in.close();
						out.close();
						clientSocket.close();
						connected = false;
						btnConnect.setText("Connect");
					} else {					
						
						clientSocket = new Socket(tbxIp.getText(), Integer
								.parseInt(tbxPort.getText()));
						out = new PrintWriter(clientSocket.getOutputStream(),
								true);
						in = new BufferedReader(new InputStreamReader(
								clientSocket.getInputStream()));
						
						connected = true;
						textArea.setText(in.readLine());
						btnConnect.setText("Disconnect");
					}
					btnPost.setEnabled(connected);
					btnGet.setEnabled(connected);
				} catch (UnknownHostException e) {
					textArea.setText("Client::" + e.getMessage());
				} catch (NumberFormatException | IOException e) {
					// TODO Auto-generated catch block
					textArea.setText("Client::" + e.getMessage());
				}

			}
		});
		btnPost.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String fromUser = "POST " + tbxPost.getText();

				try {
					// Send the post message
					textArea.setText(fromUser);
					if (fromUser != null) {
						out.println(fromUser);
					}

					handleResponse(in.readLine());
				} catch (Exception ex) {
					textArea.setText("Client::" + ex.getMessage());
				}

			}
		});
		btnGet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String fromUser = "GET " + tbxGet.getText();

				try {
					// Send the get message
					textArea.setText(fromUser);
					if (fromUser != null) {
						out.println(fromUser);
					}
					handleResponse(in.readLine());

				} catch (Exception ex) {
					textArea.setText("Client::" + ex.getMessage());
				}
			}
		});
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					if (connected){
						out.println("exit");
						in.close();
						out.close();
						clientSocket.close();
					}
				} catch (IOException e) {

				}
			}
		});
	}

	/*
	 * ErrorCode : Message | Cause ------------------------------   : <Query
	 * Result> | A query was successful Assignment 1 Protocol (A1P) 1/30/2012
	 * 8:11 PM 1 : No input | A Post or a get command was sent on its own 2
	 * : No Data | A Get command has no data to return 3 : Incorrect Command |
	 * Something other than a Post or Get command was
	 */
	private void handleResponse(String text) {

		String returnCode = text.substring(0, 1);
		text = text.substring(1);
		switch (returnCode) {
		case " ":
			String oldText = textArea.getText();
			if (oldText != "") {
				oldText += "\n";
			}
			textArea.setText(oldText + text);
			break;
		case "1":
		case "2":
		case "3":
			JOptionPane.showMessageDialog(frame, text);
			break;
		default:
			JOptionPane.showMessageDialog(frame, "UKNONWN ERROR");
			break;
		}
	}
}

package norris.cory.networks.a1;
import java.io.IOException;
import java.net.*;
import java.util.SortedMap;
import java.util.TreeMap;

public class Server {
	private static int port = 1024;
	private static int maxConnections = 0; // 0 for unlimited
	private static int currConnections = 0;

	public static void main(String[] args)
	{
		ServerSocket listener = null;
		SortedMap<String, Integer> data = new TreeMap<String, Integer> ();
		
		try {
			listener = new ServerSocket(port);
		} catch (IOException e)
		{
            System.err.println("Could not listen on port: "+port);
            System.exit(1);
		}

		try {
			while ((currConnections++ < maxConnections) || (maxConnections == 0)) {			
				new A1P(listener.accept(), data).start();		
				
			}
			listener.close();
		} catch (IOException e)
		{
			System.out.println("Exiting ioe");
			System.out.println(e.getStackTrace());
		} catch (Exception e)
		{
			System.out.println("Exiting e");
		}finally
		{
			
		}
	}

}

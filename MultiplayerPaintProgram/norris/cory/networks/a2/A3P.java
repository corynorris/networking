package norris.cory.networks.a2;
import java.awt.Color;
import java.io.BufferedInputStream;

import java.io.IOException;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.Socket;
import java.util.ArrayList;

public class A3P extends Thread{

	private Socket socket = null;
	private ArrayList<ColorPoint> points = new ArrayList<ColorPoint>();
	private ArrayList<ObjectOutputStream> clients = new ArrayList<ObjectOutputStream>();
	private ObjectInputStream in = null;
	
	/*
	 * ErrorCode : Message | Cause
		------------------------------
		  : <Query Result> | A query was successful
		Assignment 1 Protocol (A1P) 1/30/2012 8:11 PM
		1 : No input | A Post or a get command was sent on its own
		2 : No Data | A Get command has no data to return
		3 : Incorrect Command | Something other than a Post or Get command was
	 */

	
	public A3P(Socket clientSocket, ArrayList<ObjectOutputStream> clients, ArrayList<ColorPoint> points) {		
		super("A3P");
		this.socket = clientSocket;
		this.clients = clients;
		this.points = points;	
	}

	
	
	@Override
	public void run() {
		try {

			// Initialize in/out streams
			//out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));  //TODO:Make sure this works

				
			//Constantly listen for new input && send new points
			ColorPoint p =  new ColorPoint(0,0,Color.red);
			
			while ((p = (ColorPoint) in.readObject()).c != Color.WHITE)
			{
				//Add the point
				addPoint(p);
				
				//broadcast the change to all threads
				tellOthers(p);
				
			}			
			
	
			in.close();
			socket.close();
		} catch (IOException e) {
					
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/** addToMap
	 * adds a key to a map OR increments the value of they key already there by 1
	 * @param key The key to add to the map
	 */
	/**
	 * @param key
	 */
	private synchronized void addPoint(ColorPoint p)
	{		
		System.out.println(points.size());
		points.add(p); // find point
	}

	private synchronized void tellOthers(ColorPoint p)
	{
		
		for (int i = 0; i < clients.size(); i++)
		{
			try {
				clients.get(i).writeObject(p);
			}catch (IOException e)
			{
				clients.remove(i);
				i--;
			}
		}
	}

	

}

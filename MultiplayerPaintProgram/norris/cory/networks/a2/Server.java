package norris.cory.networks.a2;
import java.awt.Color;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.ArrayList;
import java.util.Random;

public class Server {
	private static int port = 1024;
	private static int maxConnections = 4; // 0 for unlimited
	private static int currConnections = 0;
	public static int MAXPOINTS = 10000;

	
	public static void main(String[] args)
	{
		ServerSocket listener = null;
		ArrayList<ColorPoint> points = new ArrayList<ColorPoint>();
		ArrayList<ObjectOutputStream> clients = new ArrayList<ObjectOutputStream>();

		try {
			listener = new ServerSocket(port);
		} catch (IOException e)
		{
            System.err.println("Could not listen on port: "+port);
            System.exit(1);
		}

		
		float h = 0;
		float s = 0;
		float b = 0;
		try {
			System.out.println("Listening, max connections = 4");
			
			while ((currConnections++ < maxConnections) || (maxConnections == 0)) {	
				Socket socket = listener.accept();
				ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream()); 
				clients.add(out);

				Random rand = new Random();
				h += rand.nextFloat();
				s = rand.nextFloat();
				b = rand.nextFloat();
				
				
				//Assign a colour
				out.writeObject(Color.getHSBColor(h, s, b));
				//send points
				out.writeObject(points);
				new A3P(socket, clients, points).start();		
				
			}
	
			listener.close();
		} catch (IOException e)
		{
			System.out.println("Exiting ioe");
			System.out.println(e.getStackTrace());
		} catch (Exception e)
		{
			System.out.println("Exiting e");
		}finally
		{
			
		}
	}

}

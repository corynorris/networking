package norris.cory.networks.a2;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JPanel;

public class NetworkedPaintPanel extends JPanel {
	private static final long serialVersionUID = -510851126566507626L;
	private boolean enabled = false;
	private Color assignedColor;
	ObjectOutputStream out;
	ObjectInputStream in;
	// array of 10000 java.awt.Point references
	private ArrayList<ColorPoint> points = new ArrayList<ColorPoint>();

	// set up GUI and register mouse event handler
	public NetworkedPaintPanel() {
		// handle frame mouse motion event
		addMouseMotionListener(new MouseMotionAdapter() {
			// store drag coordinates and repaint
			public void mouseDragged(MouseEvent event) {
				if (enabled) {
					// Create our point
					ColorPoint c = new ColorPoint(event.getPoint(),
							assignedColor);

					// Send the point to the server
					try {
						out.writeObject(c);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Add the point to our drawing, so that there is no lag
					addPoint(c);

				}
			}
		});
	}

	@SuppressWarnings("unchecked")
	public void load(Color colour, final ObjectInputStream in,
			final ObjectOutputStream out) throws ClassNotFoundException, IOException {
		this.assignedColor = colour;
		this.in = in;
		this.out = out;
		points = (ArrayList<ColorPoint>) in.readObject();
		repaint();
		enabled = true;

		// Start a thread to listen for feedback
		new Thread(new Runnable() {
			public synchronized void run() {
			
				try {
					while ( enabled ) {
						ColorPoint p = (ColorPoint) in.readObject();
						if (p.c != assignedColor && p.c != Color.WHITE) {
							points.add(p);
							repaint();
						}

					}
					out.writeObject(Color.WHITE);// Signals a close connection
				} catch (ClassNotFoundException | IOException e) {
					//This will be triggered when we close
				}
				

			}
		}).start();
	}

	public void disable() {
		enabled = false;
	}

	public void addPoint(ColorPoint p) {
		points.add(p); // find point
		repaint(); // repaint JFrame

	}

	// draw oval in a 4-by-4 bounding box at specified location on window
	public void paintComponent(Graphics g) {
		super.paintComponent(g); // clears drawing area

		// draw all points in array
		for (int i = 0; i < points.size(); i++) {
			g.setColor(points.get(i).c);
			g.fillOval(points.get(i).x, points.get(i).y, 4, 4);
		}
	} // end method paint

} // end class PaintPanel
package norris.cory.networks.a2;
import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.net.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class Client {

	private JFrame frame;
	private JTextField tbxIp;
	private JTextField tbxPort;
	private JButton btnConnect;
	private boolean connected = false;
	private Socket clientSocket = null;
	private NetworkedPaintPanel paintPanel;
	ObjectOutputStream out = null;
	ObjectInputStream in = null;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Client window = new Client();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Client() {
		initialize();
		registerActions();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);

		frame.setBounds(100, 100, 514, 484);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		// Setup Connection View
		JPanel pnlConnection = new JPanel();
		pnlConnection.setBorder(new EmptyBorder(10, 10, 10, 10));
		frame.getContentPane().add(pnlConnection, BorderLayout.NORTH);
		GridBagLayout gbl_pnlConnection = new GridBagLayout();
		pnlConnection.setLayout(gbl_pnlConnection);

		JLabel lblIp = new JLabel("Ip:");
		GridBagConstraints gbc_lblIp = new GridBagConstraints();
		gbc_lblIp.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblIp.fill = GridBagConstraints.BOTH;
		gbc_lblIp.insets = new Insets(0, 0, 5, 5);
		gbc_lblIp.gridx = 0;
		gbc_lblIp.gridy = 0;
		pnlConnection.add(lblIp, gbc_lblIp);

		tbxIp = new JTextField();
		lblIp.setLabelFor(tbxIp);
		GridBagConstraints gbc_tbxIp = new GridBagConstraints();
		gbc_tbxIp.gridx = 1;
		gbc_tbxIp.gridy = 0;
		gbc_tbxIp.fill = GridBagConstraints.BOTH;
		gbc_tbxIp.anchor = GridBagConstraints.NORTHWEST;
		gbc_tbxIp.weightx = 1.0;
		gbc_tbxIp.gridwidth = GridBagConstraints.REMAINDER;
		gbc_tbxIp.insets = new Insets(1, 1, 5, 1);
		pnlConnection.add(tbxIp, gbc_tbxIp);
		tbxIp.setColumns(10);

		JLabel lblPort = new JLabel("Port:");
		GridBagConstraints gbc_lblPort = new GridBagConstraints();
		gbc_lblPort.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblPort.fill = GridBagConstraints.BOTH;
		gbc_lblPort.insets = new Insets(0, 0, 5, 5);
		gbc_lblPort.gridx = 0;
		gbc_lblPort.gridy = 1;
		pnlConnection.add(lblPort, gbc_lblPort);

		tbxPort = new JTextField();
		lblPort.setLabelFor(tbxPort);
		GridBagConstraints gbc_tbxPort = new GridBagConstraints();
		gbc_tbxPort.gridx = 1;
		gbc_tbxPort.gridy = 1;
		gbc_tbxPort.fill = GridBagConstraints.BOTH;
		gbc_tbxPort.anchor = GridBagConstraints.NORTHWEST;
		gbc_tbxPort.weightx = 1.0;
		gbc_tbxPort.gridwidth = GridBagConstraints.REMAINDER;
		gbc_tbxPort.insets = new Insets(1, 1, 5, 1);
		pnlConnection.add(tbxPort, gbc_tbxPort);
		tbxPort.setColumns(10);

		btnConnect = new JButton((connected) ? "Disconnect" : "Connect");
		GridBagConstraints gbc_btnConnect = new GridBagConstraints();
		gbc_btnConnect.anchor = GridBagConstraints.EAST;
		gbc_btnConnect.gridx = 1;
		gbc_btnConnect.gridy = 2;
		pnlConnection.add(btnConnect, gbc_btnConnect);

		paintPanel = new NetworkedPaintPanel(); // create paint panel
		paintPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		frame.getContentPane().add(paintPanel, BorderLayout.CENTER);
	

	}

	private void registerActions() {

		// CONNECT BUTTON PRESSED
		btnConnect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {

					//If connected, disconnect.
					if (connected) {
						paintPanel.disable();						
						in.close();
						out.close();
						clientSocket.close();
						connected = false;
						btnConnect.setText("Connect");
						
					
					//If not connected, then connect
					} else {
						//Get connection parameters from GUI
						String ip = tbxIp.getText();
						Integer port = Integer.parseInt(tbxPort.getText());
						
						
						//Connect via TCP, and establish in and out streams
						clientSocket = new Socket(ip, port);
						out = new ObjectOutputStream(clientSocket.getOutputStream());
						in = new ObjectInputStream(new BufferedInputStream(clientSocket.getInputStream()));  //TODO:Make sure this works

						
						//Load the PaintPanel
						//Once connected to the server, two things are returned:
						//1: Colour
						//2: PointsArray
						
						//Get colour from the server, assign our colour
						Color c;
						connected = true;
						try {
							c = (Color) in.readObject();
							paintPanel.load(c, in, out);
							
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							JOptionPane.showMessageDialog(frame, "Incorrect response from server");
							e.printStackTrace();
						}
						
						
						
						//Let other processes know we're connected
						
						btnConnect.setText("Disconnect");
					}

				} catch (UnknownHostException e) {  //Can't Connect
					JOptionPane.showMessageDialog(frame, e.getMessage());
				} catch (NumberFormatException | IOException e) { //Can't Invalid Ip
					JOptionPane.showMessageDialog(frame, e.getMessage());
				}

			}
		});
		
		
		//Close all sockets upon exiting so that the server is properly closed
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					if (connected) {
						out.writeObject(Color.WHITE);//Signals a close connection
						in.close();
						out.close();
						clientSocket.close();
					}
				} catch (IOException e) {

				}
			}
		});
	}


}

package norris.cory.networks.a2;
import java.awt.Color;
import java.awt.Point;


public class ColorPoint extends Point {
	private static final long serialVersionUID = -6060587934143609243L;
	public Color c;
	
	public ColorPoint(int x, int y, Color c)
	{
		super(x,y);
		this.c = c;
	}
	public ColorPoint (Point p, Color c)
	{
		super(p);
		this.c = c;
	}
}
